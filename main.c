#include <stdio.h>

typedef struct Cube
{
    char y[8]; 
    char g[8]; 
    char o[8]; 
    char b[8]; 
    char r[8]; 
    char w[8]; 
} Cube;

void init_empty_cube(Cube *cube);
void print_cube(Cube cube);
void input_seed(Cube *cube, char *seed);
void turn_cube(Cube *cube, char* turn);

int main(void)
{
    Cube cube;
    init_empty_cube(&cube);
    print_cube(cube);
    return 0;
}

void init_empty_cube(Cube *cube)
{
    int i;
    for (i = 0; i < 48; i++)
        cube->y[i] = ' ';
}

void print_cube(Cube cube)
{
    printf("\
    -----\n\
    |%c%c%c|\n\
    |%cy%c|\n\
    |%c%c%c|\n\
-----------------\n\
|%c%c%c|%c%c%c|%c%c%c|%c%c%c|\n\
|%cg%c|%co%c|%cb%c|%cr%c|\n\
|%c%c%c|%c%c%c|%c%c%c|%c%c%c|\n\
-----------------\n\
    |%c%c%c|\n\
    |%cw%c|\n\
    |%c%c%c|\n\
    -----\n\
",
cube.y[0], cube.y[1], cube.y[2], cube.y[3], cube.y[4], cube.y[5], cube.y[6], cube.y[7],
cube.g[0], cube.g[1], cube.g[2], cube.g[3], cube.g[4], cube.g[5], cube.g[6], cube.g[7],
cube.o[0], cube.o[1], cube.o[2], cube.o[3], cube.o[4], cube.o[5], cube.o[6], cube.o[7],
cube.b[0], cube.b[1], cube.b[2], cube.b[3], cube.b[4], cube.b[5], cube.b[6], cube.b[7],
cube.r[0], cube.r[1], cube.r[2], cube.r[3], cube.r[4], cube.r[5], cube.r[6], cube.r[7],
cube.w[0], cube.w[1], cube.w[2], cube.w[3], cube.w[4], cube.w[5], cube.w[6], cube.w[7]
);
}

void input_seed(Cube *cube, char *seed)
{
    int i;
    for (i = 0; i < 48; i++)
        cube->y[i] = seed[i];
}

void turn_cube(Cube *cube, char* turn)
{
    char tmp;
    if (strcmp(turn, "y") == 0)
    {
    }
    else if (strcmp(turn, "y'") == 0)
    {
    }
    else if (strcmp(turn, "y2") == 0)
    {
    }
    else if (strcmp(turn, "g") == 0)
    {
    }
    else if (strcmp(turn, "g'") == 0)
    {
    }
    else if (strcmp(turn, "g2") == 0)
    {
    }
    else if (strcmp(turn, "o") == 0)
    {
    }
    else if (strcmp(turn, "o'") == 0)
    {
    }
    else if (strcmp(turn, "o2") == 0)
    {
    }
    else if (strcmp(turn, "b") == 0)
    {
    }
    else if (strcmp(turn, "b'") == 0)
    {
    }
    else if (strcmp(turn, "b2") == 0)
    {
    }
    else if (strcmp(turn, "r") == 0)
    {
    }
    else if (strcmp(turn, "r'") == 0)
    {
    }
    else if (strcmp(turn, "r2") == 0)
    {
    }
    else if (strcmp(turn, "w") == 0)
    {
    }
    else if (strcmp(turn, "w'") == 0)
    {
    }
    else if (strcmp(turn, "w2") == 0)
    {
    }
}
/*
    -----
    |   |
    | y |
    |   |
-----------------
|   |   |   |   |
| g | o | b | r |
|   |   |   |   |
-----------------
    |   |
    | w |
    |   |
    -----
*/
